import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import type { RootState } from '../store'

export const tagTypes = ['Person'] as const

/** initialize an empty api service that we'll inject endpoints into later as needed */
export const api = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://test.onesteprelief.org/onestep/',

    prepareHeaders: (headers, api) => {
      const state = api.getState() as RootState
      const token = state.auth.token || localStorage.getItem('authtoken')
      if (token) {
        headers.set('Authorization', `Bearer ${token}`)
      }
      return headers
    }
  }),

  endpoints: () => ({}),

  tagTypes
})
