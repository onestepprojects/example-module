import type { Address, Picture, SocialMedia } from './common'

export interface Person {
  uuid: string
  name: string
  currentAddress: Address
  email: string
  phones: {
    phone: string
  }
  birthdate: string
  profilePicture: Picture
  lastActivity: string
  history: any[]
  homeAddress: Address
  ecUUID: string
  ecName: any
  ecPhones: {
    phone: string
  }
  ecAddress: Address
  paymentAccount: {
    payID: string
    blockchainAddress: string
    status: string
    assets: any
  }
  location: any
  gender: string
  socialMedia: SocialMedia
}
