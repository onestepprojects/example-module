import { createSlice, type PayloadAction } from '@reduxjs/toolkit'
import type { Person } from '../types'

interface AuthState {
  token?: string
  person?: Person
  roles?: string[]
  isAdmin?: boolean
}

const initialState: AuthState = {}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setAuth: (state, action: PayloadAction<Pick<AuthState, 'token' | 'roles' | 'person'>>) => {
      const { token, person, roles } = action.payload

      // TODO: Consider remove this. RTK query read token from redux.
      if (token) {
        localStorage.setItem('authtoken', token)
      }

      state.token = token
      state.roles = roles
      state.person = person

      state.isAdmin = roles?.includes('admin')
    }
  }
})
