// eslint-disable-next-line no-use-before-define
import React, { FC, useEffect } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { useAppDispatch, authSlice, type Person } from './store'

export interface ExampleContainerProps extends RouteComponentProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const ExampleContainer: FC<ExampleContainerProps> = ({ auth }) => {
  // TODO: Remove this after refactor all components
  // const mountPath = ''

  const dispatch = useAppDispatch()

  useEffect(() => {
    if (!auth) return
    dispatch(authSlice.actions.setAuth(auth))
  }, [auth])

  return (
    <div className='ui container'>
      <h1>Hello {auth.person.name}!</h1>
    </div>
  )
}
