// eslint-disable-next-line no-use-before-define
import React, { FC } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store } from './store'
import { ExampleContainer, ExampleContainerProps } from './Container'

interface ExampleModuleProps extends ExampleContainerProps {
  /** Module router base name. */
  mountPath: string
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  authHelper?: any
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  person?: any
}

const ExampleModule: FC<ExampleModuleProps> = ({ mountPath, ...restProps }) => {
  return (
    <BrowserRouter basename={mountPath}>
      <Provider store={store}>
        <ExampleContainer {...restProps} />
      </Provider>
    </BrowserRouter>
  )
}

export default ExampleModule
