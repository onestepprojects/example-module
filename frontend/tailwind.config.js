/** @type {import('@onestepprojects/frontend-infrastructure').TailwindConfig} */
module.exports = {
  presets: [require('@onestepprojects/frontend-infrastructure/tailwind.config')],
}
