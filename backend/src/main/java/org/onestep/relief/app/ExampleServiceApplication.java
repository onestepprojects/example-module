package org.onestep.relief.app;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class ExampleServiceApplication extends Application<ExampleServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new ExampleServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "ExampleService";
    }

    @Override
    public void initialize(final Bootstrap<ExampleServiceConfiguration> bootstrap) {
        // TODO: application initialization

        bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
            bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false)));
    }

    @Override
    public void run(final ExampleServiceConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    }

}
